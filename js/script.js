/*TODO: @Samu
        Elimina
        Unisci
        Dividi
        funzione per capire se il nuovo step uscirà 
       fuori dallo spazio consentito dalla riga
 */

var r;
var e;
var op0=[];
var ind0=0;

// definizioni di malti, luppoli e lieviti per test. Successivamente verranno presi dal database

 malti = [
        { name : 'Maris Otter', ebc : 3.2 },
        { name : 'Crystal', ebc  : 160 },
        { name : 'Brown Malt', ebc  : 200 }
      ];
 luppoli = [
        {name : 'EKG', type : 'Pellet',  aa : 5.9, time : 60, after_hot_break : true}
        
      ];
  lieviti = { name: 'WYeast London ESB', type : 'liquid', attenuation : 83}; 
  // la homebrewlib non supporta ancora l'aggiunta di più lieviti, quindi non supporta ancora gli array, solamente l'aggiunta di un oggetto
  
  acque = [{name : 'acqua1'}, {name: 'acqua2'}];

  spezie = [{name: 'spezia1'}, {name: 'spezia2'}];

  zuccheri = [{name: 'zucchero1', type:'Sucrose'},{name: 'zucchero2', type:'Sucrose'}];




$(document).ready(function(){


 r=homebrewlib;
 e= r.newRecipe();
op0[0]=0;



e.reset();
nuovopara(0,0);




});

var stepeseguiti = 0;

var numerorighe = 0;
const deci = 1000;









function compamenu(num){

  console.log("ho cliccato su uno spazio non definito");

$( "#primomenu"+num ).css( "display", "inline" );
  $("#speciale"+num).css("height","378px");




}
var i=0;
function mostramenuint(num){

  $( "#menuintint"+num ).css( "display", "inline" );
}

function creaStep(nriga){


  

var mainDiv = document.getElementById("riga"+nriga);
var div = document.createElement("div");
var clAttr = document.createAttribute("class");
var idAttr = document.createAttribute("id");
idAttr.value="stepdef"+stepeseguiti;
clAttr.value="def azione";
div.setAttributeNode(clAttr);

div.setAttributeNode(idAttr);

mainDiv.appendChild(div);





}


function mettistep(n,nriga,split,step){ // funzione che viene eseguita quando si sceglie uno step


// MODIFICA DELLA FRECCIA CHE RICHIAMA LA FUNZIONE AGGIUNGI STEP PER CAMBIARGLI I MENU
var alt="";  
var nome;
var altro;
var mui=0;
var ind=10000-(step*nriga);
var spl='<a style="background-color:white; z-index:'+ind+';" onclick="split('+step+','+nriga+',1,0,0)" id="split" class="item">Dividi</a>';
var spazi="<br><br><br>";
//modifica il menu della freccia chiamante
if(split===0)
{
  if (step===0)
    nome="freccia0";
  else
    nome="freccia";
altro="";}
if(split==1)
{nome="split1";
alt="height:117px;";
spl='';
altro="speciale"+step;
mui=1;
ind=9;
spazi="<br>\
      <br>\
      <br>\
      <br><br>";}
if(split==2)
{nome="split2";
alt="";
spl='<a style="background-color:white; z-index:'+ind+';" onclick="split('+step+','+nriga+',1,0,1)" id="split" class="item">Dividi</a>';
altro="speciale"+step;}

if(split==3)
{nome="split3";
alt="height:117px;";
spl='';
spazi="<br>\
      \
      <br><br><br><br>";
altro="speciale"+step;}


  $("#spazfre"+step).html('<img id="freccia'+step+'" onclick="compapara('+step+',1)" style="margin-top:1px;'+alt+'" class="freccia" src="simboli/'+nome+'.png"  >\
      '+spazi+'\
      <div id="secondomenu'+step+'" style=" background-color:white; z-index:'+ind+'; display:none;" class="ui secondary vertical menu menuint2" >\
                    \
                    <a style="background-color:white; z-index:'+ind+';  " onclick="compapara('+step+',1)" id="modify" class="item">Modifica</a>\
'+spl+'</div>         \
          ');
var valo=0;
  valo=stepeseguiti+deci;
$("#spazfre"+step).attr('onmouseleave','scomparemenu2('+step+','+mui+')');
$("#spazfre"+step).attr('onmouseover','compamenu2('+step+','+mui+')');
$("#spazfre"+step).attr('class','confre '+altro);

// A SECONDA DELLO STEP, ESEGUI LA RELATIVA AZIONE

if(n==1)
  {
  e.mash();


}
  if(n==2)
  {
    e.boil();
  }
  if(n==3)
  {
    e.ferment();}
  if(n==4)
  {
    e.bottle();}


// SE LE OPERAZIONI VENGONO SVOLTE SULLA PRIMA RIGA, CONSERVA LE OPERAZIONI [SAREBBE DA FARE SU TUTTE
// PERO' PER MANCANZA DI TEMPO CI SIAMO CONCENTRATI SOLAMENTE SULLA PRIMA]
if(nriga===0)
{
  ind0++;
  op0[ind0] = n

}







//controlla la larghezza della colonna per allargare la riga
var riga = $('#riga'+nriga).width();
var sinistra=$('#spazfre'+step).offset();
if ((sinistra.left+300)>riga)
{
  
  nuovo=riga+600;
  $('#riga'+nriga).css('width',nuovo+'px');
}


// CREO UNO SPAZIO PER UNA NUOVA ATTIVITA E NE DEFINISCO LE RELATIVE PROPRIETA', INFINE GLI ASSEGNO IL CODICE HTML
creaStep(nriga);
$("#stepdef"+stepeseguiti).html('<img style="border=0px;" id="step'+stepeseguiti+'" onclick="compapara('+stepeseguiti+',0)" src="simboli/'+n+'.png" width="95" height="57">  \
  <div id="secondomenu'+valo+'" style="z-index:1; display:none;" class="ui secondary vertical menu menuint2" >\
                    \
                    <a style="background-color:white; z-index:1;" onclick="compapara('+stepeseguiti+',0)" id="modify" class="item">Modifica</a>\
              </div>');
  
  
  $("#stepdef"+stepeseguiti).attr('width','100');
  $("#stepdef"+stepeseguiti).attr('height','59');
  $("#stepdef"+stepeseguiti).attr('class','def azione');
  
  $("#stepdef" + stepeseguiti).attr('onmouseover','compamenu2('+valo+',0)');
  $("#stepdef" + stepeseguiti).attr('onmouseleave','scomparemenu2('+valo+',0)');


// CREO UN NUOVO SPAZIO PER CONTENERE I PARAMETRI DI QUESTO STEP
  nuovopara(stepeseguiti + deci, n);



// AUMENTO IL NUMERO DEGLI STEP ESEGUITI
  stepeseguiti=stepeseguiti+1;
  
ind=10000-(stepeseguiti*nriga);
//inserisci(nriga);

  

  





//scomparsa del menù richiamato
scompamenu(nriga);

// creazione della freccia successiva



var mainDiv = document.getElementById("riga"+nriga);

var div = document.createElement("div");
var clas = document.createAttribute("class");
var i = document.createAttribute("id");
var mouOv = document.createAttribute("onmouseover");
var mouLea = document.createAttribute("onmouseleave");
clas.value="confre ";
i.value="spazfre"+stepeseguiti;
mouOv.value="compamenu("+stepeseguiti+")";
mouLea.value="scompamenu("+stepeseguiti+",0)";
div.setAttributeNode(clas);
div.setAttributeNode(i);
div.setAttributeNode(mouLea);
div.setAttributeNode(mouOv);
mainDiv.appendChild(div);


var nuovo='<img style="border:0px;" onclick="compapara('+stepeseguiti+',1)" class="freccia" id="freccia'+stepeseguiti+'" src="simboli/freccia.png"  >\
      \
      <br>\
      <br>\
      <br>\
     \
      <div id="primomenu'+stepeseguiti+'" style="display:none; z-index:'+ind+';" class="ui secondary vertical menu menuint" >\
          <div style="background-color:white; z-index:'+ind+';" class="ui dropdown item" onmouseout="chiudimenuint('+stepeseguiti+')" onmouseover="mostramenuint('+stepeseguiti+')" >\
                <i class="dropdown icon"></i>\
                aggiungi\
                <div id="menuintint'+stepeseguiti+'"  class="menu menuint" style="display:none;" >\
                    <a onclick="mettistep(1,'+nriga+',0,'+stepeseguiti+')"  class="item">Ammostamento</a>\
                    <a onclick="mettistep(2,'+nriga+',0,'+stepeseguiti+')"  class="item">Bollitura</a>\
                    <a onclick="mettistep(3,'+nriga+',0,'+stepeseguiti+')"  class="item">Fermentazione</a>\
\                   <a onclick="mettistep(4,'+nriga+',0,'+stepeseguiti+')"  class="item">Imbottigliamento</a>\
                \
              </div>\
               \
          </div>\
         <a style="z-index:'+ind+'; background-color:white;" onclick="split('+stepeseguiti+','+nriga+',0,0,0)" id="split" class="item">Dividi</a>\
         \
         <a style="z-index:'+ind+'; background-color:white;" onclick="compapara('+stepeseguiti+',1)" id="modify" class="item">Modifica</a>\               </div>\
        \
      </div>';





$('#spazfre'+stepeseguiti).html(nuovo);



 
// CREO UN NUOVO SPAZIO DOVE CONTENERE LE CARATTERISTICHE DEL FLUSSO IN USCITA DALL'ATTIVITA'

nuovopara(stepeseguiti,n);
scompara();


}


/*function inserisci(num){

  if (num===0){
    RIGA0.push(stepeseguiti);
  }

  if (num==1){
    RIGA1.push(stepeseguiti);
  }

  if (num==2){
    RIGA2.push(stepeseguiti);
  }

  if (num==3){
    RIGA3.push(stepeseguiti);
  }

  if (num==4){
    RIGA4.push(stepeseguiti);
  }

  if (num==5){
    RIGA5.push(stepeseguiti);
  }

  if (num==6){
    RIGA6.push(stepeseguiti);
  }

  if (num==7){
    RIGA7.push(stepeseguiti);
  }



}*/

function scompamenu(num,mo){
  $( "#primomenu"+num).css( "display", "none" );
  if(mo==1)
  {$("#spazfre"+num).css("height","218px");}

}




function compamenu2(numero,spe)
{
  $( "#secondomenu"+numero).css( "display", "inline" );
if (spe==1)
  {$("#spazfre"+numero).css("height","238px");}
}


function split(num,nriga,dopo,divide, ricevediviso){
  //IMPLEMENTARE LO SPLIT CON 2 PARAMETRI: DIVISO, RISULTATO  DIVISO. 
numerorighe=numerorighe+1;
scomparemenu2(num);
scompara();
console.log("richiamata funzione di split");


var ind=10000-(num*nriga);


//posizionamento 
var nuovo;

//modifico la freccia esistente in una freccia di split

if (ricevediviso===0)
{
    if (divide===0){

      if (dopo===0){
                  nuovo='<img class="freccia" onclick="compapara('+num+',1)" id="freccia'+num+'" src="simboli/split1.png"><br><br><br>\
                  <br>\
                  <br>\
                  <br> <div id="primomenu'+num+'"  style="width:95px; z-index:'+ind+'; display: none;" class="ui menupart secondary vertical menu menuint bubu">          <div class="ui dropdown item" style=" background-color:white; z-index:9; padding:0 0 0 10; margin:0px; width: 90px; height:30px;" onmouseout="chiudimenuint('+num+')" onmouseover="mostramenuint('+num+')">                <i class="dropdown icon"></i>     Aggiungi<div id="menuintint'+num+'" class="menu menuint" style="display: none;">                    <a onclick="mettistep(1,'+nriga+',1,'+num+')"  class="item">Ammostamento</a>                    <a onclick="mettistep(2,'+nriga+',1,'+num+')"  class="item">Bollitura</a>                    <a onclick="mettistep(3,'+nriga+',1,'+num+')"  class="item">Fermentazione</a>    <a onclick="mettistep(4,'+nriga+',1,'+num+')"  class="item">Imbottigliamento</a>            </div>          </div>   <a style="z-index:3; margin-bottom:0px; background-color:white;" onclick="compapara('+num+',1)" id="modify" class="item">Modifica</a>     </div>';
            }
      else{
                   nuovo='<img class="freccia" onclick="compapara('+num+',1)" id="freccia'+num+'" src="simboli/split1.png"><br><br><br>\
                        <br>\
                        <br>\
                        <br> <div id="secondomenu'+num+'" style="background-color:white; z-index:3; display:none;" class="ui secondary vertical menu menuint2" >\
                                <a style="z-index:'+ind+'; margin-bottom:0px; background-color:white; " onclick="compapara('+num+',1)" id="modify" class="item">Modifica</a>\
                                </div>';

          }      
    }
    else{

        // DIVISO NON SARA MAI UGUALE AD 1 PERCHE' NON RICHIAMEREBBE LA FUNZIONE SI SPLIT

    }

}

else{
var altez= $('#vuoto'+nriga).width();
  $('#vuoto'+nriga).css('width',altez-10);
  if(divide===0){


      if (dopo===0){
                     nuovo='<img class="freccia" style="margin-top:1px;" onclick="compapara('+num+',1)" id="freccia'+num+'" src="simboli/split3.png"><br><br><br><br><br>\
                          <br> <div id="primomenu'+num+'"  style="width:95px; z-index:'+ind+'; display: none;" class="ui menupart secondary vertical menu menuint bubu">          <div class="ui dropdown item" style="background-color:white; z-index:'+ind+'; padding:0 0 0 10; margin:0px; width: 90px; height:30px;" onmouseout="chiudimenuint('+num+')" onmouseover="mostramenuint('+num+')">                <i class="dropdown icon"></i>     Aggiungi<div id="menuintint'+num+'" class="menu menuint" style="display: none;">                    <a onclick="mettistep(1,'+nriga+',3,'+num+')"  class="item">Ammostamento</a>                    <a onclick="mettistep(2,'+nriga+',3,'+num+')" class="item">Bollitura</a>                    <a onclick="mettistep(3,'+nriga+',3,'+num+')" class="item">Fermentazione</a>    <a onclick="mettistep(4,'+nriga+',3,'+num+')"  class="item">Imbottigliamento</a>            </div>          </div>    <a style="z-index:'+ind+'; margin-bottom:0px; background-color:white; " onclick="compapara('+num+',1)" id="modify" class="item">Modifica</a>             </div>';
      }
      else
      {
       nuovo='<img class="freccia" style="margin-top:1px;" onclick="compapara('+num+',1)" id="freccia'+num+'" src="simboli/split3.png"><br><br><br><br><br><br>            <br> <div id="secondomenu'+num+'" style="z-index:'+ind+'; display:none;" class="ui secondary vertical menu menuint2" >\              <a style="z-index:'+ind+'; margin-bottom:0px; background-color:white; " onclick="compapara('+num+',1)" id="modify" class="item">Modifica</a></div>';              
      }    
 }}


$('#spazfre'+num).html(nuovo);
$('#spazfre'+num).css('height','117px');
$('#spazfre'+num).css('width','90px');
$('#spazfre'+num).css('float','left');
$('#freccia'+num).css('height','117px');

$('#spazfre'+num).attr("class",'confre speciale'+num);

//creazione del menu in posizione speciale per la freccia split
 /*    var menu=' <div id="primomenu'+num+'" style="display:none; width:100px;" class="ui menupart secondary vertical menu menuint bubu" >\
          <div class="ui dropdown item" onmouseout="chiudimenuint('+num+')" onmouseover="mostramenuint('+num+')" >\
                <i class="dropdown icon bubu"></i>\
                aggiungi\
                <div id="menuintint'+num+'"  class="menu menuint" style="display:none;" >\
                    <a onclick="mettistep(1,'+nriga+',1,'+num+')" id="step1" class="item">1</a>\
                    <a onclick="mettistep(2,'+nriga+',1,'+num+')" id="step2" class="item">2</a>\
                    <a onclick="mettistep(3,'+nriga+',1,'+num+')" id="step3" class="item">3</a>                </div>\
          </div>\
          <a onclick="split('+stepeseguiti+','+nriga+')" id="split" class="item">Dividi</a>\
          <a onclick="merge('+stepeseguiti+','+nriga+')" id="merge" class="item">Unisci</a>\
        </div>\
        \
      </div>';

var rig = document.getElementById("riga"+nriga);
var nuovDiv = document.createElement("div");
var idD = document.createAttribute("id");
var clD = document.createAttribute("class");
var styleD = document.createAttribute("style");
var ov = document.createAttribute("onmouseover");
var le = document.createAttribute("onmouseleave");
clD.value="idbox";
//styleD.value="height:147px; width:110px; margin-top:60px; float:left; border:0; display:inline;";
styleD.value="float:left; border:0; position: relative; top: 60px; display:inline;";

idD.value="id"+stepeseguiti;
le.value="scompamenu("+stepeseguiti+")";
ov.value="compamenu("+stepeseguiti+")";
nuovDiv.setAttributeNode(styleD);
nuovDiv.setAttributeNode(idD);
nuovDiv.setAttributeNode(le);
nuovDiv.setAttributeNode(ov);
nuovDiv.setAttributeNode(clD);

rig.appendChild(nuovDiv);

$('#id'+stepeseguiti).html(menu);

*/

//Creazione di un div per contenere un'altra riga di lavoro.
var posizione= $("#freccia"+num).offset();


var nuovaRiga = document.createElement("div");
var clRiga = document.createAttribute("class");
var idRiga = document.createAttribute("id");
idRiga.value="riga"+numerorighe;
clRiga.value="riga";
nuovaRiga.setAttributeNode(clRiga);
nuovaRiga.setAttributeNode(idRiga);

var mainDiv = document.getElementById("principale");

mainDiv.appendChild(nuovaRiga);
document.getElementById("riga"+numerorighe).style.position = "static";
//var valo= "RIGA"+numerorighe;

//nella nuova riga, inizializzo la freccia proveniente da uno split
stepeseguiti=stepeseguiti+1;
var ind=10000-(stepeseguiti*numerorighe);

//inizializzo l'HTML della freccia che dovrà comparire
var nuovo2='<img onclick="compapara('+stepeseguiti+',1)" style="margin-top:1px;" class="freccia" id="freccia'+stepeseguiti+'" src="simboli/split2.png"  > <br>      <br>      <br>      <div id="primomenu'+stepeseguiti+'" style="display:none;" class="ui secondary vertical menu menuint" >        <div style="background-color:white;" class="ui dropdown item" onmouseout="chiudimenuint('+stepeseguiti+')" onmouseover="mostramenuint('+stepeseguiti+')" >                <i class="dropdown icon"></i>              aggiungi                <div id="menuintint'+stepeseguiti+'"  class="menu menuint" style="display:none;" >                    <a onclick="mettistep(1,'+numerorighe+',2,'+stepeseguiti+')" class="item">Ammostamento</a>                    <a onclick="mettistep(2,'+numerorighe+',2,'+stepeseguiti+')"  class="item">Bollitura</a>                    <a onclick="mettistep(3,'+numerorighe+',2,'+stepeseguiti+')" class="item">Fermentazione</a>  <a onclick="mettistep(4,'+numerorighe+',2,'+stepeseguiti+')" class="item">Imbottigliamento</a>              </div>          </div>               <a style="z-index:3; margin-bottom:0px; background-color:white;" onclick="compapara('+stepeseguiti+',1)" id="modify" class="item">Modifica</a>          <a style="background-color:white;" onclick="split('+stepeseguiti+','+numerorighe+',0,0,1)" id="split" class="item">Dividi</a>      </div>      </div>';

//aumento l'altezza dello spazio di lavoro
var al=$('#principale').height();
$('#principale').css('height',al+160);

//crea uno spazio vuoto per generare l'offset
mainDiv = document.getElementById("riga"+numerorighe);
var nuoDiv = document.createElement("div");
var idVuo = document.createAttribute("id");
var styVuo = document.createAttribute("style");
idVuo.value="vuoto"+numerorighe;
var posiz= posizione.left;
//var posiz= posizione.left-10;

styVuo.value ="width:"+posiz+"px; height:90px; float:left; visibility:hidden;";
nuoDiv.setAttributeNode(idVuo);
nuoDiv.setAttributeNode(styVuo);
mainDiv.appendChild(nuoDiv);





//prima creo il div della freccia

var nuovaFre = document.createElement("div");
var clRiga2 = document.createAttribute("class");
var idRiga2 = document.createAttribute("id");
idRiga2.value="spazfre"+stepeseguiti;
clRiga2.value="confre";


var mouLeav = document.createAttribute("onmouseleave");
var mouOve = document.createAttribute("onmouseover");
mouLeav.value = "scompamenu("+stepeseguiti+',0)';
mouOve.value = "compamenu("+stepeseguiti+')';

nuovaFre.setAttributeNode(clRiga2);
nuovaFre.setAttributeNode(idRiga2);
nuovaFre.setAttributeNode(mouOve);
nuovaFre.setAttributeNode(mouLeav);


mainDiv.appendChild(nuovaFre);

$('#spazfre'+stepeseguiti).html(nuovo2);





var pa = $('#para'+num+' h3:first').text();

//nuovopara(stepeseguiti, pa); NON ANCORA SUPPORTATO



//var vil=valo.findIndex(trova(num));


//document.getElementById("riga"+numerorighe).style.left = posizione+60+"px";

//creazione di un nuovo step





}

/*function trova(numero,num){

  return numero==num;

};*/

function merge(num){

  scomparemenu2(num);
  scompara();
  console.log("richiamata funzione di merge");
}



/*

FUNZIONE CHE NON VIENE MAI CHIAMATA. FUNZIONA SOLAMENTE DAL PUNTO DI VISTA GRAFICO.

function deletestep(num){
  scomparemenu2(num);
  scompara();
  console.log("richiamata funzione di delete step");


    var list = document.getElementById("step"+num);
    numero=num-1;
    $(list).css("display","none");
    $("#freccia"+numero).css("display","none");

}
*/







function scompara(){
  $('img').css('border','0px');
  $(".pluto").css("display", "none");
}

function chiudimenuint(nriga){
$( "#menuintint"+nriga ).css( "display", "none" );

}

function scomparemenu2(num){

$( "#secondomenu"+num).css( "display", "none" );
 $("#spazfre"+num).css("height","218px");


}


function compapara(num,freccia){

    scomparemenu2(num);
    scompamenu(num,0);
    scompara();
    
//funzione per implementare l'evidenziazione degli elementi. 
//BISOGNA METTERE BORDI SULLE IMMAGINI, NON SUI DIV
//METTERE GLI ID SULLE IMMAGINI

    if (freccia===0)
     {var param=num+deci;
      $("#step"+num).css( "border", "3px solid yellow" );
      
      $("#para"+param).css( "display", "block" );}
   else
    {$("#para"+num).css( "display", "block" );
      $("#freccia"+num).css( "border", "3px solid yellow" );}
}

function termina(n){

$("#stepnodef"+n).html('<img src="simboli/end.png" height=55px>');



}

function nuovopara(num,step){

var nomepasso="";
var costanti="";  

  var pippo = document.getElementById("pippo");
  var par = document.createElement("div");
  var clAttr = document.createAttribute("class");
  var idAttr = document.createAttribute("id");
  idAttr.value="para" + num;
  clAttr =document.createAttribute("class");
  clAttr.value ="pluto";
  var style = document.createAttribute("style");
  style.value='display:none; height: 100px; border-style: none;';
  par.setAttributeNode(style);
  par.setAttributeNode(idAttr);
  par.setAttributeNode(clAttr);
  pippo.appendChild(par);
  var nuovoparametro;
  nomepasso=step;
  var risul;
  if (num >=1000)
  {

 risul=num-1000;
var costanti="";
if(step==1)
  {nomepasso="Ammostamento";
   e.process.mash = {malts:[], hops:[], yeast:[], spice:[], sparge_water:0};

   // NELL'AMMOSTAMENTO, L'UNICA COSTANTE DA RICHIEDERE E' QUANTA ACQUA USARE PER LO SPARGING
   costanti='<div id="acque'+risul+'" class="pos">\
<h4>Acqua: </h4>\
  <h5> Aggiungi Acqua</h5>\
  <div class="aggiungi">\
  <div class="perbottone">\
  <div class="pa">\
  <p style="width:110px; float:left;">Quantità:</p>\
  <input type="number" min="0" step="0.1" value=0 style="width:60px" id = "acquaquant'+num+'">\
  </div>\
<div class="pa">\
  <p style="width:110px; float:left;">Unità di misura:</p>\
  <form>\
  <select id="acquaunità'+num+'">\
      <option value="l">litri</option>\
\
    <option value="gal">galloni</option>\
\
  </select>\
  </form>\
  </div>\
  </div>\
  <button style="margin-top:10px;" onclick="mettingrediente('+num+',1,4)">Aggiungi acqua</button>\
   \
  \
  </div>\
  <div style="display:none;" id="acqueagg'+risul+'"> \
<h5> Acque Aggiunte </h5>\
<div class="contenitore" id="contenitoreacque'+risul+'">\
</div>\
</div>\
  </div>';

 }
if(step==2)
  {nomepasso="Bollitura";
  e.process.boil = {malts:[], hops:[], yeast:[], spice:[], time:0, water_addition:0, sugar_addition:{}};
   costanti='<div id="tempobollitura'+risul+'" class="pos2">\
\
  <h5> Tempo di bollitura totale</h5>\
  <div class="aggiungi">\
  <div class="perbottone">\
  <div class="pa">\
  <p style="width:110px; float:left;">Tempo:</p>\
  <input type="number" min="0" step="1" value=0 style="width:60px" id = "tempoquant'+num+'">\
  </div>\
<div class="pa">\
  <p style="width:110px; float:left;">Unità di misura:</p>\
  <form>\
  <select id="tempounità'+num+'">\
      <option value="min">minuti</option>\
\
    <option value="d">giorni</option>\
\
  </select>\
  </form>\
  </div>\
  </div>\
  <button style="margin-left:5px; margin-top:10px; width:100px;" onclick="mettingrediente('+num+',2,6)">Aggiorna tempo</button>\
   \
  \
  </div>\
\
  </div>\
  <div id="acque'+risul+'" class="pos2">\
\
  <h5> Acqua per diluire</h5>\
  <div class="aggiungi">\
  <div class="perbottone">\
  <div class="pa">\
  <p style="width:110px; float:left;">Quantità:</p>\
  <input type="number" min="0" step="0.1" value=0 style="width:60px" id = "acquaquant'+num+'">\
  </div>\
<div class="pa">\
  <p style="width:110px; float:left;">Unità di misura:</p>\
  <form>\
  <select id="acquaunità'+num+'">\
      <option value="l">litri</option>\
\
    <option value="gal">galloni</option>\
\
  </select>\
  </form>\
  </div>\
  </div>\
  <button style="width:90px; margin-top:10px;" onclick="mettingrediente('+num+',2,4)">Aggiorna acqua</button> \
   \
  \
  </div>\
  <div style="display:none;" id="acqueagg'+risul+'"> \
<h5> Acque Aggiunte </h5>\
<div class="contenitore" id="contenitoreacque'+risul+'">\
</div>\
</div>\
  </div>\
<div id="zuccheri'+risul+'" class="pos2">\
\
  <h5> Aggiungi zucchero per dolcificare</h5>\
  <div class="aggiungi">\
  <div class="perbottone">\
  \
  <div class="pa">\
  <p style="width:110px; float:left;">Quantità:</p>\
  <input type="number" min="0" step="0.1" value=0 style="width:60px" id = "zuccheroquant'+num+'">\
  </div>\
<div class="pa">\
  <p style="width:110px; float:left;">Unità di misura:</p>\
  <form>\
  <select id="zuccherounità'+num+'">\
      <option value="kg">kilogrammi</option>\
\
    <option value="lbs">pound</option>\
\
  </select>\
  </form>\
  </div>\
  </div>\
  <button style="margin-top:10px; margin-left:25px; width:80px; " onclick="mettingrediente('+num+',2,7)">Aggiorna zucchero</button> \
   \
  \
  </div>\
  \
  </div>';
/*
INSERIRE QUESTO CODICE PER VISUALIZZARE LA LISTA DEGLI ZUCCHETI PRIMA DELLA RICHIESTA DI QUANTITA'

<div class="pa" ><p style="width:110px; float:left;">Seleziona zucchero:</p> \
  <form>\
  <select id="zucchero'+num+'">\
   \
  </select>\
  </form>\
  </div>\

 */



}
if(step==3)
  {nomepasso="Fermentazione";
  e.process.ferment = {malts:[], hops:[], yeast:[], spice:[], temperature:0};
  costanti='<div id="temperature'+risul+'" class="pos">\
  <h5> Temperatura di fermentazione</h5>\
  <div class="aggiungi">\
  <div class="perbottone">\
  <div class="pa">\
  <p style="width:110px; float:left;">Quantità:</p>\
  <input type="number" min="0" step="1" value=0 style="width:60px" id = "temperaturaquant'+num+'">\
  </div>\
<div class="pa">\
  <p style="width:110px; float:left;">Unità di misura:</p>\
  <form>\
  <select id="temperaturaunità'+num+'">\
      <option value="°C">gradi</option>\
\
    <option value="F">Fahrenheit</option>\
\
  </select>\
  </form>\
  </div>\
  </div>\
  <button style="margin-top:10px;" onclick="mettingrediente('+num+',3,8)">Aggiorna temperatura</button>\
   \
  \
  </div>\
\
  </div>';

}
if(step==4)
  {nomepasso="Imbottigliamento";
  e.process.prime = {malts:[], hops:[], yeast:[], spice:[], sugar:0};
costanti='<div id="zuccheri'+risul+'" class="pos2">\
\
  <h5> Aggiungi zucchero per dolcificare</h5>\
  <div class="aggiungi">\
  <div class="perbottone">\
  \
  <div class="pa">\
  <p style="width:110px; float:left;">Quantità:</p>\
  <input type="number" min="0" step="0.1" value=0 style="width:60px" id = "zuccheroquant'+num+'">\
  </div>\
<div class="pa">\
  <p style="width:110px; float:left;">Unità di misura:</p>\
  <form>\
  <select id="zuccherounità'+num+'">\
      <option value="g">grammi</option>\
\
    <option value="oz">once</option>\
\
  </select>\
  </form>\
  </div>\
  </div>\
  <center><button style="margin-top:10px;" onclick="mettingrediente('+num+',4,7)">Aggiorna zucchero</button></center> \
   \
  \
  </div>\
  \
  </div>';}
/*
INSERIRE QUESTO CODICE PER VISUALIZZARE LA LISTA DEGLI ZUCCHETI PRIMA DELLA RICHIESTA DI QUANTITA'

<div class="pa" ><p style="width:110px; float:left;">Seleziona zucchero:</p> \
  <form>\
  <select id="zucchero'+num+'">\
   \
  </select>\
  </form>\
  </div>\

 */




  nuovoparametro = '<h3>'+nomepasso+'</h3><div id="lieviti'+risul+'" class="pos">\
  \
  <h4>Lievito: </h4>\
  <h5> Aggiungi Lieviti</h5>\
  <div class="aggiungi">\
  <div class="pa" ><p style="width:110px; float:left;">Seleziona lievito:</p> \
  <form>\
  <select id="lievito'+num+'">\
    \
  </select>\
  </form>\
  </div>\
  \
 \
  \
  <center><button style="margin-top:10px;" onclick="mettingrediente('+num+','+step+',1)">Aggiungi lievito</button></center> \
   \
  \
  <div class="aggiunt" style="display:none;" id="lievitiagg'+risul+'"> \
<h5> Lieviti Aggiunti </h5>\
<div class="contenitore" id="contenitorelieviti'+risul+'">\
</div>\
</div>\
  </div>\
\
\
  </div>\
\
  \
  \
  <div id="malti'+risul+'" class="pos">\
<h4>Malto: </h4>\
  <h5> Aggiungi Malti</h5>\
  <div class="aggiungi">\
  <div class="pa" ><p style="width:110px; float:left;">Seleziona malto:</p> \
  <form>\
  <select id="malto'+num+'">\
   \
  </select>\
  </form>\
  </div>\
  \
  <div class="pa">\
  <p style="width:110px; float:left;">Quantità:</p>\
  <input min="0" type="number" step="0.1" value=0 style="width:60px" id = "maltoquant'+num+'">\
  </div>\
<div class="pa">\
  <p style="width:110px; float:left;">Unità di misura:</p>\
  <form>\
  <select id="maltounità'+num+'">\
      <option value="kg">kilogrammi</option>\
\
    <option value="lbs">pound</option>\
   \
  </select>\
  </form>\
  </div>\
  \
  <center><button style="margin-top:10px;" onclick="mettingrediente('+num+','+step+',2)">Aggiungi malto</button></center> \
   \
  \
  </div>\
  <div style="display:none;" id="maltiagg'+risul+'"> \
<h5> Malti Aggiunti </h5>\
<div class="aggiunt" class="contenitore" id="contenitoremalti'+risul+'">\
</div>\
</div>\
  </div>\
  \
  \
  <div id="luppoli'+risul+'" class="pos">\
<h4>Luppolo: </h4>\
  <h5> Aggiungi Luppolo</h5>\
  <div class="aggiungi">\
  <div class="pa" ><p style="width:120px; float:left;">Seleziona luppolo:</p> \
  <form>\
  <select id="luppolo'+num+'">\
\
  </select>\
  </form>\
  </div>\
  \
  <div class="pa">\
  <p style="width:110px; float:left; width:120px; ">Quantità:</p>\
  <input type="number" min="0" step="0.1" value=0 style="width:60px" id = "luppoloquant'+num+'">\
  </div>\
<div class="pa">\
  <p style="width:120px; float:left;">Unità di misura:</p>\
  <form>\
  <select id="luppolounità'+num+'">\
    <option value="g">grammi</option>\
    <option value="oz">once</option>\
\
  </select>\
  </form>\
  </div>\
  <div class="pa">\
  <p style="width:120px;  float:left;">Tempo:</p>\
  <input type="number" min="0" step="1" value=0 style="width:60px" id = "luppolotempo'+num+'">\
\
  </div>\
  <div class="pa">\
  <p style="width:120px;  float:left;">Unità di misura:</p>\
  <form>\
  <select id="luppolotempounità'+num+'">\
    <option value="min">minuti</option>\
    <option value="d">giorni</option>\
\
  </select>\
  </form>\
  </div>\
  \
  <center><button style="margin-top:10px;" onclick="mettingrediente('+num+','+step+',3)">Aggiungi luppolo</button></center> \
   \
  \
  </div>\
  <div style="display:none;" id="luppoliagg'+risul+'"> \
<h5> Luppoli Aggiunti </h5>\
<div class="aggiunt" class="contenitore" id="contenitoreluppoli'+risul+'">\
</div>\
</div>\
  </div>\
  \
  \
  \
  \
  \
  <div id="spezie'+risul+'" class="pos">\
<h4>Spezie: </h4>\
  <h5> Aggiungi spezia</h5>\
  <div class="aggiungi">\
  <div class="pa" ><p style="width:110px; float:left;">Seleziona spezia:</p> \
  <form>\
  <select id="spezia'+num+'">\
\
  </select>\
  </form>\
  </div>\
  \
  <div class="pa">\
  <p style="width:110px; float:left;">Quantità:</p>\
  <input type="number" min="0" step="0.1" value=0 style="width:60px" id = "speziaquant'+num+'">\
  </div>\
<div class="pa">\
  <p style="width:110px; float:left;">Unità di misura:</p>\
  <form>\
  <select id="speziaunità'+num+'">\
    <option value="g">grammi</option>\
    <option value="oz">once</option>\
\
  </select>\
  </form>\
  </div>\
  \
  <center><button style="margin-top:10px;" onclick="mettingrediente('+num+','+step+',5)">Aggiungi spezia</button></center> \
   \
  \
  </div>\
  <div style="display:none;" id="spezieagg'+risul+'"> \
<h5> Spezie Aggiunti </h5>\
<div class="aggiunt" class="contenitore" id="contenitorespezie'+risul+'">\
</div>\
</div>\
  </div>\
<div style="width:290px;" id="costanti'+risul+'" class="pos"> <h4>Configurazione per l\'attività di '+nomepasso+'</h4>'+costanti+'</div>';
;






}
else{





  nuovoparametro = '<h3> '+e.state[num].name+'</h3><br>\
     <p> Volume: '+(e.state[num].vol).toFixed(3)+' </p>\
     <p> Densità iniziale: '+e.state[num].og.toFixed(3)+' </p>\
     <p> Densità finale: '+e.state[num].fg.toFixed(3)+' </p>\
     <p> Alcol (vol): '+e.state[num].abv.toFixed(1)+' </p>\
     <p> Colore (EBC): '+e.state[num].ebc.toFixed(0)+' </p>\
     <p> Amarezza (IBU): '+e.state[num].ibu.toFixed(0)+' </p>\
     <p> Carbonazione (g/l): '+e.state[num].co2.toFixed(1)+' </p>\
     <button onclick="modPara('+num+')" >Modifica parametri</button> ';





     
           
}
$("#para"+num).html(nuovoparametro);


if (num>=1000)
{
// carica lieviti
 
          var mainDiv = document.getElementById("lievito"+num);
          var div = document.createElement("option");
          var vaAttr = document.createAttribute("text");
          var idAttr = document.createAttribute('id');
          idAttr.value="option"+0+"lievito"+num;
          vaAttr.value=  lieviti.name;
          div.setAttributeNode(idAttr);
          div.setAttributeNode(vaAttr);
          mainDiv.appendChild(div);
          $('#option'+0+"lievito"+num).html(lieviti.name);

      
      // carico i malti
  for (var i=0; i<malti.length; i++)
      {
          var mainDiv = document.getElementById("malto"+num);
          var div = document.createElement("option");
          var vaAttr = document.createAttribute("text");
          var idAttr = document.createAttribute('id');
          idAttr.value="option"+i+"malto"+num;
          vaAttr.value=  malti[i].name;
          div.setAttributeNode(idAttr);
          div.setAttributeNode(vaAttr);
          mainDiv.appendChild(div);
          $('#option'+i+"malto"+num).html(malti[i].name);

      }
      // carico i luppoli
  for (var i=0; i<luppoli.length; i++)
      {
          var mainDiv = document.getElementById("luppolo"+num);
          var div = document.createElement("option");
          var vaAttr = document.createAttribute("text");
          var idAttr = document.createAttribute('id');
          idAttr.value="option"+i+"luppolo"+num;
          vaAttr.value=  luppoli[i].name;
          div.setAttributeNode(idAttr);
          div.setAttributeNode(vaAttr);
          mainDiv.appendChild(div);
          $('#option'+i+"luppolo"+num).html(luppoli[i].name);

      }    
      // carico le acque - NON SUPPORTATE DALLA LIBRERIA    
  
      // carico le spezie
    for (var i=0; i<spezie.length; i++)
      {
          var mainDiv = document.getElementById("spezia"+num);
          var div = document.createElement("option");
          var vaAttr = document.createAttribute("text");
          var idAttr = document.createAttribute('id');
          idAttr.value="option"+i+"spezia"+num;
          vaAttr.value=  spezie[i].name;
          div.setAttributeNode(idAttr);
          div.setAttributeNode(vaAttr);
          mainDiv.appendChild(div);
          $('#option'+i+"spezia"+num).html(spezie[i].name);

      }  
/* ATTUALMENTE NON è RICHIESTO DI CARICARE I VARI TIPI DI ZUCCHERO
      // se siamo in una fase di bollitura
      if ((step==2)||(step==4))
      {// carico gli zuccheri
      for (var i=0; i<zuccheri.length; i++)
      {
          var mainDiv = document.getElementById("zucchero"+num);
          var div = document.createElement("option");
          var vaAttr = document.createAttribute("text");
          var idAttr = document.createAttribute('id');
          idAttr.value="option"+i+"zucchero"+num;
          vaAttr.value=  zuccheri[i].name;
          div.setAttributeNode(idAttr);
          div.setAttributeNode(vaAttr);
          mainDiv.appendChild(div);
          $('#option'+i+"zucchero"+num).html(zuccheri[i].name);

      }

    } 
*/
}



}



function modPara(num){

nuovoparametro = '<h3> '+e.state[num].name+'</h3><br>\
      Volume  : <input type="number" style="width:70px" value="'+e.state[num].vol+'" id = "vol'+num+'"><br>\
      Densità iniziale   : <input type="number" style="width:70px" value="'+e.state[num].og+'" id = "og'+num+'"><br>\
      Densità finale   : <input type="number" style="width:70px" value="'+e.state[num].fg+'" id = "fg'+num+'"><br>\
      Alcol (vol)  : <input type="number" style="width:70px" value="'+e.state[num].abv+'" id = "abv'+num+'"><br>\
      Colore (EBC)  : <input type="number" style="width:70px" value="'+e.state[num].ebc+'" id = "ebc'+num+'"><br>\
      Amarezza (IBU)  : <input type="number" style="width:70px" value="'+e.state[num].ibu+'" id = "ibu'+num+'"><br>\
      Carbonazione (g/l)  : <input type="number" style="width:70px" value="'+e.state[num].co2+'" id = "co2'+num+'">\
      </p>\
      <button onclick="conPara('+num+')" >Conferma parametri</button>      ';
      $("#para"+num).html(nuovoparametro);


}

function conPara(num){

 
    
     e.state[num]=
    { name : e.state[num].name,
      vol: Number($("#vol"+num).val()),
      og: Number($("#og"+num).val()), 
      fg: Number($("#fg"+num).val()),
      abv: Number($("#abv"+num).val()),
      ebc: Number($("#ebc"+num).val()),
      ibu: Number($("#ibu"+num).val()),
      co2: Number($("#co2"+num).val())
    };

    var nuovoparametro = '<h3> '+e.state[num].name+'</h3><br>\
               <p> Volume: '+(e.state[num].vol).toFixed(3)+' </p>\
               <p> Densità iniziale: '+(e.state[num].og).toFixed(3)+' </p>\
               <p> Densità finale: '+(e.state[num].fg).toFixed(3)+' </p>\
               <p> Alcol (vol): '+(e.state[num].abv).toFixed(1)+' </p>\
               <p> Colore (EBC): '+(e.state[num].ebc).toFixed(0)+' </p>\
               <p> Amarezza (IBU): '+(e.state[num].ibu).toFixed(0)+' </p>\
               <p> Carbonazione (g/l): '+(e.state[num].co2).toFixed(1)+' </p>\
               <button onclick="modPara('+num+')" >Modifica parametri</button> ';

              $("#para"+i).html(nuovoparametro);


aggiornaValori(num);








     $("#para"+num).html(nuovoparametro);
}


function  aggiornaValori(num){
 //ciclo per eliminare gli step fino al passo modificato
 var lun= op0.length;
  for ( var i=(num+1); i<lun; i++ )
    {
        e.undo();
    }

    for ( var i=(num+1); i<lun; i++ )
    {
        var step = op0[i];
        if (step==1)
          e.mash();
        if (step==2)
          e.boil();
        if (step==3)
          e.ferment();
        if (step==4)
          e.bottle();



    }

    for (var i=(num+1); i<lun; i++)
    {

         var nuovoparametro = '<h3> '+e.state[i].name+'</h3><br>\
               <p> Volume: '+(e.state[i].vol).toFixed(3)+' </p>\
               <p> Densità iniziale: '+(e.state[i].og).toFixed(3)+' </p>\
               <p> Densità finale: '+(e.state[i].fg).toFixed(3)+' </p>\
               <p> Alcol (vol): '+(e.state[i].abv).toFixed(1)+' </p>\
               <p> Colore (EBC): '+(e.state[i].ebc).toFixed(0)+' </p>\
               <p> Amarezza (IBU): '+(e.state[i].ibu).toFixed(0)+' </p>\
               <p> Carbonazione (g/l): '+(e.state[i].co2).toFixed(1)+' </p>\
               <button onclick="modPara('+i+')" >Modifica parametri</button> ';

              $("#para"+i).html(nuovoparametro);


    }
    



}



function mettingrediente(n,step, ingrediente){
var risul=n-1000;

if (ingrediente==1)
{
        // è stato aggiunto un lievito

          $('#lievitiagg'+risul).css('display','inline');

          var mainDiv = document.getElementById("contenitorelieviti"+risul);
          var div = document.createElement("div");
          var idAttr = document.createAttribute("id");
          var clAttr = document.createAttribute("class");
          var numero= (document.getElementById('contenitorelieviti'+risul).childElementCount);
          idAttr.value="lievito"+n+"aggiunto"+numero;
          clAttr.value ="aggiunto";

          div.setAttributeNode(idAttr);
          div.setAttributeNode(clAttr);
          mainDiv.appendChild(div);
          //document.getElementById("demo").innerHTML = lieviti0.value;
            var x;

          var liev= document.getElementById('lievito'+n);
          var index= liev.selectedIndex;


          
           numero= (document.getElementById('contenitorelieviti'+risul).childElementCount)-1;

            var str= '<p style="float:left; width:200px;" >  '+liev.options[index].text+'</p>\
            <img style="width:19px; height:19px;" onclick="rimuoviingrediente('+step+','+numero+','+ingrediente+',0,'+n+')" src="simboli/delete.png">'; 
            $("#lievito"+n+'aggiunto'+numero).html(str);
            //document.getElementById("lievito"+n+'aggiunto'+quanlieviti).innerHTML = str;
            var operazione=lieviti;
            if (step==1)
            
              e.process.mash.yeast=operazione;
            
            if (step==2)
              e.process.boil.yeast=operazione;
            if (step==3)
              e.process.ferment.yeast=operazione;
            if (step==4)
              e.process.prime.yeast=operazione;


}

if (ingrediente==2)
{
    // è stato aggiunto un malto

      $('#maltiagg'+risul).css('display','inline');
          var numero= (document.getElementById('contenitoremalti'+risul).childElementCount);

          var mainDiv = document.getElementById("contenitoremalti"+risul);
          var div = document.createElement("div");
          var idAttr = document.createAttribute("id");
          var clAttr = document.createAttribute("class");

          idAttr.value="malto"+n+"aggiunto"+numero;
          clAttr.value ="aggiunto";

          div.setAttributeNode(idAttr);
          div.setAttributeNode(clAttr);
          mainDiv.appendChild(div);
          //document.getElementById("demo").innerHTML = lieviti0.value;
            var x;

          var mal= document.getElementById('malto'+n);
          var index= mal.selectedIndex;

          var quanmal= document.getElementById('maltoquant'+n);

          var malunità= document.getElementById('maltounità'+n);
          var indexuni= malunità.selectedIndex;

           numero= (document.getElementById('contenitoremalti'+risul).childElementCount)-1;

            var str= '<p style="width:200px; float:left;" >  '+mal.options[index].text+ 'Qta. '+quanmal.value+ malunità.options[indexuni].value+'</p><img style="width:19px; height:19px;" onclick="rimuoviingrediente('+step+','+numero+','+ingrediente+',0,'+n+')" src="simboli/delete.png">'; 
            $("#malto"+n+'aggiunto'+numero).html(str);
            //document.getElementById("lievito"+n+'aggiunto'+quanlieviti).innerHTML = str;

            // CONTROLLA L'UNITA' DI MISURA E NE EFFETTUA LA CONVERSIONE PER INSERIRLO NELLA LIBRERIA
            var valore= quanmal.value;
            if (malunità.options[indexuni].value=="lbs")
              valore= lbs2kg(quanmal.value);



          var operazione= {name: malti[index].name, weight: Number(valore), ebc: malti[index].ebc};
            if (step==1)
              e.process.mash.malts.push(operazione);
            if (step==2)
              e.process.boil.malts.push(operazione);
            if (step==3)
              e.process.ferment.malts.push(operazione);
            if (step==4)
              e.process.prime.malts.push(operazione);

}


if (ingrediente==3)
{
    // è stato aggiunto un luppolo

    $('#luppoliagg'+risul).css('display','inline');
          var numero= (document.getElementById('contenitoreluppoli'+risul).childElementCount);

          var mainDiv = document.getElementById("contenitoreluppoli"+risul);
          var div = document.createElement("div");
          var idAttr = document.createAttribute("id");
          var clAttr = document.createAttribute("class");

          idAttr.value="luppolo"+n+"aggiunto"+numero;
          clAttr.value ="aggiunto";

          div.setAttributeNode(idAttr);
          div.setAttributeNode(clAttr);
          mainDiv.appendChild(div);
          //document.getElementById("demo").innerHTML = lieviti0.value;
            var x;

          var lup= document.getElementById('luppolo'+n);
          var index= lup.selectedIndex;

          var quanlup= document.getElementById('luppoloquant'+n);

          var lupunità= document.getElementById('luppolounità'+n);
          var indexuni= lupunità.selectedIndex;


            

            var valore= quanlup.value;
            if (lupunità.options[indexuni].value=="oz")
              valore= oz2g(quanlup.value);

            var tempolup = document.getElementById('luppolotempo'+n);

            var tempolupuni= document.getElementById('luppolotempounità'+n);
            var indextempo = tempolupuni.selectedIndex;
           numero= (document.getElementById('contenitoreluppoli'+risul).childElementCount)-1;

            var str= '<p style="width:200px; float:left;">  '+lup.options[index].text+ 'Qta. '+quanlup.value+ lupunità.options[indexuni].value+'Tempo:'+tempolup.value+tempolupuni.options[indextempo].value+'</p><img style="width:19px; height:19px;" onclick="rimuoviingrediente('+step+','+numero+','+ingrediente+',0,'+n+')" src="simboli/delete.png">'; 
            $("#luppolo"+n+'aggiunto'+numero).html(str);

            var tempo=tempolup.value;
            if (tempolupuni.options[indextempo].value=="d")
              tempo= tempolup.value*3600;




            //document.getElementById("lievito"+n+'aggiunto'+quanlieviti).innerHTML = str;
            var operazione= {name : luppoli[index].name, weight : Number(valore), time : Number(tempo), type : luppoli[index].type,  aa : luppoli[index].aa ,  after_hot_break : luppoli[index].after_hot_break};
            if (step==1)
              e.process.mash.hops.push(operazione);
            if (step==2)
              e.process.boil.hops.push(operazione);
            if (step==3)
              e.process.ferment.hops.push(operazione);
            if (step==4)
              e.process.prime.hops.push(operazione);

}

if (ingrediente==4)
{
    // è stata aggiunta l'acqua

          var numero= (document.getElementById('contenitoreacque'+risul).childElementCount);

          var mainDiv = document.getElementById("contenitoreacque"+risul);
          var div = document.createElement("div");
          var idAttr = document.createAttribute("id");
          var clAttr = document.createAttribute("class");

          idAttr.value="acqua"+n+"aggiunto"+numero;
          clAttr.value ="aggiunto";

          div.setAttributeNode(idAttr);
          div.setAttributeNode(clAttr);
          mainDiv.appendChild(div);
          //document.getElementById("demo").innerHTML = lieviti0.value;
            var x;

          

          var quanacq= document.getElementById('acquaquant'+n);

          var acqunità= document.getElementById('acquaunità'+n);
          var indexuni= acqunità.selectedIndex;
           numero= (document.getElementById('contenitoreacque'+risul).childElementCount)-1;


            

            var valore= quanacq.value;
            if (acqunità.options[indexuni].value=="gal")
              valore= gal2l(quanacq.value);

            var str= '<p style="width:200px; float:left;" >  Qta. '+quanacq.value+ acqunità.options[indexuni].value+'</p><img style="width:19px; height:19px;" onclick="rimuoviingrediente('+step+','+numero+','+ingrediente+','+valore+','+n+')" src="simboli/delete.png">'; 
            $("#acqua"+n+'aggiunto'+numero).html(str);

            //document.getElementById("lievito"+n+'aggiunto'+quanlieviti).innerHTML = str;
          
            if (step==1)
              {e.process.mash.sparge_water=e.process.mash.sparge_water+Number(valore);
              $('#acqueagg'+risul).css('display','inline');}
            if (step==2)
              e.process.boil.water_addition=Number(valore);
            //if (step==3)
             // e.process.ferment.water.push(operazione);
            //if (step==4)
             // e.process.prime.water.push(operazione);

}

if (ingrediente==5)
{
    // è stata aggiunta una spezia

    $('#spezieagg'+risul).css('display','inline');
          var numero= (document.getElementById('contenitorespezie'+risul).childElementCount);

          var mainDiv = document.getElementById("contenitorespezie"+risul);
          var div = document.createElement("div");
          var idAttr = document.createAttribute("id");
          var clAttr = document.createAttribute("class");

          idAttr.value="spezia"+n+"aggiunto"+numero;
          clAttr.value ="aggiunto";

          div.setAttributeNode(idAttr);
          div.setAttributeNode(clAttr);
          mainDiv.appendChild(div);
          //document.getElementById("demo").innerHTML = lieviti0.value;
            var x;

          var spe= document.getElementById('spezia'+n);
          var index= spe.selectedIndex;

          var quanspe= document.getElementById('speziaquant'+n);

          var speunità= document.getElementById('speziaunità'+n);
          var indexuni= speunità.selectedIndex;

           numero= (document.getElementById('contenitorespezie'+risul).childElementCount)-1;

            var str= '<p style="width:200px; float:left;" >  '+spe.options[index].text+ 'Qta. '+quanspe.value+ speunità.options[indexuni].value+'</p><img style="width:19px; height:19px;" onclick="rimuoviingrediente('+step+','+numero+','+ingrediente+',0,'+n+')" src="simboli/delete.png">'; 
            $("#spezia"+n+'aggiunto'+numero).html(str);

            var valore= quanspe.value;
            if (speunità.options[indexuni].value=="oz")
              valore= oz2g(quanspe.value);

            //document.getElementById("lievito"+n+'aggiunto'+quanlieviti).innerHTML = str;
          var operazione= {name: spezie[index].name, weight: Number(valore)};
            if (step==1)
              e.process.mash.spice.push(operazione);
            if (step==2)
              e.process.boil.spice.push(operazione);
            if (step==3)
              e.process.ferment.spice.push(operazione);
            if (step==4)
              e.process.prime.spice.push(operazione);

}

if(ingrediente==6)
{

         // aggiungo il tempo totale di bollitura

          var quantem= document.getElementById('tempoquant'+n);

          var temunità= document.getElementById('tempounità'+n);
          var indexuni= temunità.selectedIndex;
          var tempo=quantem.value;
            if (temunità.options[indexuni].value=="d")
              tempo= quantem.value*3600;

          e.process.boil.time= Number(tempo);

}

if (ingrediente==7)
{
    // aggiungo zucchero


          //var zuc= document.getElementById('zucchero'+n);
          //var index= zuc.selectedIndex;

          var quanzuc= document.getElementById('zuccheroquant'+n);

          var zucunità= document.getElementById('zuccherounità'+n);
          var indexuni= zucunità.selectedIndex;
          var zucchero=quanzuc.value;
            

          if (step==2)
            {if (zucunità.options[indexuni].value=="lbs")
              zucchero= lbs2kg(quanzuc.value);
            //NON RICHIESTO IL NOME, QUANDO VERRà IMPLEMENTATA NELLA LIBRERIA LA PARTE RELATIVA ALLO ZUCCHERO, ALLORA COMMENTARE LA RIGA DI SOTTO E DECOMMENTARE QUESTA
//          e.process.boil.sugar_addition= {name: zuccheri[index].name , type:zuccheri[index].type,  qty: Number(zucchero)};
          e.process.boil.sugar_addition= {type:'Sucrose',  qty: Number(zucchero)};}

          if (step==4)
            {if (zucunità.options[indexuni].value=="oz")
              zucchero= oz2g(quanzuc.value);
            // passo solamente la quantità perchè il resto non è implementato nella libreria
          e.process.prime.sugar=Number(zucchero);}
}

if (ingrediente==8)
{
  // aggiorno il tempo di fermentazione
  var quantemp= document.getElementById('temperaturaquant'+n);

          var tempunità= document.getElementById('temperaturaunità'+n);
          var indexuni= tempunità.selectedIndex;
          var temperatura=quantemp.value;
            if (tempunità.options[indexuni].value=="F")
              temperatura= f2c(quantemp.value);

          e.process.ferment.temperature= Number(temperatura);

}
  

  





aggiornaValori(risul);




}

function rimuoviingrediente(n,quantità, ingrediente,valore,numero){
  
if (ingrediente==1)
  {nome=("lievito").concat(numero);
  
  if (n==1)
    e.process.mash.yeast[quantità]={attenuation: 0};
  if (n==2)
    e.process.boil.yeast[quantità]={attenuation: 0};
  if (n==3)
    e.process.ferment.yeast[quantità]={attenuation: 0};
  if (n==4)
    e.process.prime.yeast[quantità]={attenuation: 0};

}
if (ingrediente==2)
  {nome=("malto").concat(numero);
   if (n==1)
    e.process.mash.malts[quantità]={weight: 0, ebc:0};
  if (n==2)
    e.process.boil.malts[quantità]={weight: 0, ebc:0};
  if (n==3)
    e.process.ferment.malts[quantità]={weight: 0, ebc:0};
  if (n==4)
    e.process.prime.malts[quantità]={weight: 0, ebc:0}; 

}
if (ingrediente==3)
  {nome=("luppolo").concat(numero);
if (n==1)
    e.process.mash.hops[quantità]={weight: 0, aa:0, time:0};
  if (n==2)
    e.process.boil.hops[quantità]={weight: 0, aa:0, time:0};
  if (n==3)
    e.process.ferment.hops[quantità]={weight: 0, aa:0, time:0};
  if (n==4)
    e.process.prime.hops[quantità]={weight: 0, aa:0, time:0}; }
if (ingrediente==4)
  {nome=("acqua").concat(numero);
if (n==1)
    e.process.mash.sparge_water=e.process.mash.sparge_water-valore;}
  //if (n==2)
   // e.process.boil.water_addition=e.process.mash.sparge_water-valore; NON CAPITA
  //if (n==3)
    //e.process.mash.sparge_water=e.process.mash.sparge_water-valore; NON RICHIESTO
  //if (n==4)
   // e.process.mash.sparge_water=e.process.mash.sparge_water-valore; NON RICHIESTO
   
if (ingrediente==5)
  {nome=("spezia").concat(numero);
if (n==1)
    e.process.mash.spice[quantità]={weight: 0};
  if (n==2)
    e.process.boil.spice[quantità]={weight: 0};
  if (n==3)
    e.process.ferment.spice[quantità]={weight: 0};
  if (n==4)
    e.process.prime.spice[quantità]={weight: 0}; }

    $("#"+nome+'aggiunto'+quantità).css('display','none');

    aggiornaValori(numero-1000);

}



/*
function stampalievito()
{
  var x;
  x = document.getElementById("lieviti0").value();
  document.getElementById("demo").innerHTML = x;
}*/